## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Salesforce. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Salesforce.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Salesforce. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getLocalizations(locale, callback)</td>
    <td style="padding:15px">Returns object of localized text</td>
    <td style="padding:15px">{base_path}/{version}/localizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIncidents(startTime, service, instance, limit, offset, sort, order, locale, callback)</td>
    <td style="padding:15px">Returns an array of Incident objects. Defaults to last 30 days.</td>
    <td style="padding:15px">{base_path}/{version}/incidents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIncidentsid(id, locale, callback)</td>
    <td style="padding:15px">Returns an Incident based on Id.</td>
    <td style="padding:15px">{base_path}/{version}/incidents/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIncidentsfields(locale, callback)</td>
    <td style="padding:15px">Returns an array of Incident fields.</td>
    <td style="padding:15px">{base_path}/{version}/incidents/fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIncidentsimpactTypes(locale, callback)</td>
    <td style="padding:15px">Returns an array of incident imapct type fields.</td>
    <td style="padding:15px">{base_path}/{version}/incidents/impactTypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIncidentseventTypes(locale, callback)</td>
    <td style="padding:15px">Returns an array of incident imapct type fields.</td>
    <td style="padding:15px">{base_path}/{version}/incidents/eventTypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMaintenances(startTime, service, instance, name, limit, offset, sort, order, locale, callback)</td>
    <td style="padding:15px">Returns an array of Maintenance objects. Defaults to last 30 days.</td>
    <td style="padding:15px">{base_path}/{version}/maintenances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMaintenancesid(id, locale, callback)</td>
    <td style="padding:15px">Returns a Maintenance based on Id.</td>
    <td style="padding:15px">{base_path}/{version}/maintenances/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMaintenancesfields(locale, callback)</td>
    <td style="padding:15px">Returns an array of Maintenance fields.</td>
    <td style="padding:15px">{base_path}/{version}/maintenances/fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMaintenanceseventTypes(locale, callback)</td>
    <td style="padding:15px">Returns an array of maintenance imapct type fields.</td>
    <td style="padding:15px">{base_path}/{version}/maintenances/eventTypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGeneralMessages(limit, offset, callback)</td>
    <td style="padding:15px">Returns an array of GeneralMessage objects.</td>
    <td style="padding:15px">{base_path}/{version}/generalMessages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGeneralMessagesid(id, callback)</td>
    <td style="padding:15px">Returns a GeneralMessage based on Id.</td>
    <td style="padding:15px">{base_path}/{version}/generalMessages/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMetricValues(startTime, endTime, metricName, instanceKey, callback)</td>
    <td style="padding:15px">Returns an array of MetricValue objects.</td>
    <td style="padding:15px">{base_path}/{version}/metricValues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMetricValuesid(id, callback)</td>
    <td style="padding:15px">Returns a MetricValue based on Id.</td>
    <td style="padding:15px">{base_path}/{version}/metricValues/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLocales(locale, callback)</td>
    <td style="padding:15px">Return all locales</td>
    <td style="padding:15px">{base_path}/{version}/locales?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServices(locale, callback)</td>
    <td style="padding:15px">Return all locales</td>
    <td style="padding:15px">{base_path}/{version}/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInstances(sort, order, products, tags, childProducts, locale, callback)</td>
    <td style="padding:15px">Return all locales</td>
    <td style="padding:15px">{base_path}/{version}/instances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInstancesstatus(products, childProducts, locale, callback)</td>
    <td style="padding:15px">DEPRECATED WARNING ROUTE WILL BE REMOVED
Use /instance/status/preview for overall current status and
currently ongoing incidents, maintenances, and general messages.
Use /instances/{key}/status for current status and recent
history of incidents, maintenances, and general messages for
sepcific instance

Return all instances and associated incidents</td>
    <td style="padding:15px">{base_path}/{version}/instances/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInstancesstatuspreview(products, childProducts, locale, callback)</td>
    <td style="padding:15px">Return all instances and associated incidents, maintenances, and general messages.
For each instance returns a state enum indicating the most
severe ongoing event, and if it effects a core service.

Status Enums
* OK
* MAJOR_INCIDENT_CORE
* MINOR_INCIDENT_CORE
* MAINTENANCE_CORE
* MAJOR_INCIDENT_NONCORE
* MINOR_INCIDENT_NONCORE
* MAINTENANCE_NONCORE</td>
    <td style="padding:15px">{base_path}/{version}/instances/status/preview?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInstanceskeystatuspreview(key, productKey, childProducts, locale, callback)</td>
    <td style="padding:15px">Rerurn an instance with associated incidents, maintenances, and general messages.
For a given instance returns a state enum indicating the most
severe ongoing event, and if it effects a core service.

Status Enums
* OK
* MAJOR_INCIDENT_CORE
* MINOR_INCIDENT_CORE
* MAINTENANCE_CORE
* MAJOR_INCIDENT_NONCORE
* MINOR_INCIDENT_NONCORE
* MAINTENANCE_NONCORE</td>
    <td style="padding:15px">{base_path}/{version}/instances/{pathv1}/status/preview?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInstanceskeystatus(key, productKey, childProducts, locale, callback)</td>
    <td style="padding:15px">Return an by instance with associated incidents, maintenances, and general messages.
Returns a state enum indicating the most
severe ongoing event, and if it effects a core service.

Status Enums:
* OK
* MAJOR_INCIDENT_CORE
* MINOR_INCIDENT_CORE
* MAINTENANCE_CORE
* MAJOR_INCIDENT_NONCORE
* MINOR_INCIDENT_NONCORE
* MAINTENANCE_NONCORE</td>
    <td style="padding:15px">{base_path}/{version}/instances/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInstanceAliaseskeystatus(key, callback)</td>
    <td style="padding:15px">Return an by instance with associated incidents, maintenances, and general messages.
Returns a state enum indicating the most
severe ongoing event, and if it effects a core service.

Status Enums:
* OK
* MAJOR_INCIDENT_CORE
* MINOR_INCIDENT_CORE
* MAINTENANCE_CORE
* MAJOR_INCIDENT_NONCORE
* MINOR_INCIDENT_NONCORE
* MAINTENANCE_NONCORE</td>
    <td style="padding:15px">{base_path}/{version}/instanceAliases/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInstanceAliaseskey(key, callback)</td>
    <td style="padding:15px">Get Instance Alias</td>
    <td style="padding:15px">{base_path}/{version}/instanceAliases/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProducts(callback)</td>
    <td style="padding:15px">Return all catergories</td>
    <td style="padding:15px">{base_path}/{version}/products?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProductskey(key, callback)</td>
    <td style="padding:15px">Returns an product by id.</td>
    <td style="padding:15px">{base_path}/{version}/products/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSubscribe(XSendNotification, body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/subscribe?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchUnsubscribe(body, token, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/unsubscribe?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUnsubscribe(body, token, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/unsubscribe?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLogin(XSendNotification, body, callback)</td>
    <td style="padding:15px">Requests to send a login link to the email address</td>
    <td style="padding:15px">{base_path}/{version}/login?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogout(token, callback)</td>
    <td style="padding:15px">Logout authenticated user.</td>
    <td style="padding:15px">{base_path}/{version}/logout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubscribers(token, normalize, callback)</td>
    <td style="padding:15px">Retrieve Subscriber</td>
    <td style="padding:15px">{base_path}/{version}/subscribers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchSubscribers(XSendNotification, token, body, callback)</td>
    <td style="padding:15px">Update Subscriber</td>
    <td style="padding:15px">{base_path}/{version}/subscribers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSubscriberssubscriptionsid(id, token, body, callback)</td>
    <td style="padding:15px">Creates a Subscription for the User.</td>
    <td style="padding:15px">{base_path}/{version}/subscribers/subscriptions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchSubscriberssubscriptionsid(id, token, body, callback)</td>
    <td style="padding:15px">Updates a Subscription for the User.</td>
    <td style="padding:15px">{base_path}/{version}/subscribers/subscriptions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSubscriberssubscriptionsid(id, token, body, callback)</td>
    <td style="padding:15px">Deletes a Subscription for the User.</td>
    <td style="padding:15px">{base_path}/{version}/subscribers/subscriptions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSearchkey(key, callback)</td>
    <td style="padding:15px">Return all search results</td>
    <td style="padding:15px">{base_path}/{version}/search/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTags(callback)</td>
    <td style="padding:15px">Return all the tags</td>
    <td style="padding:15px">{base_path}/{version}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTagsinstanceinstanceKey(instanceKey, instances, callback)</td>
    <td style="padding:15px">Return all the tags for a given Instance Key</td>
    <td style="padding:15px">{base_path}/{version}/tags/instance/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTagTypes(callback)</td>
    <td style="padding:15px">Return all the tag types</td>
    <td style="padding:15px">{base_path}/{version}/tagTypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
