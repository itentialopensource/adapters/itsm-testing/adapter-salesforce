
## 0.10.4 [10-15-2024]

* Changes made at 2024.10.14_21:31PM

See merge request itentialopensource/adapters/adapter-salesforce!19

---

## 0.10.3 [09-16-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-salesforce!17

---

## 0.10.2 [08-15-2024]

* Changes made at 2024.08.14_19:51PM

See merge request itentialopensource/adapters/adapter-salesforce!16

---

## 0.10.1 [08-07-2024]

* Changes made at 2024.08.06_21:56PM

See merge request itentialopensource/adapters/adapter-salesforce!15

---

## 0.10.0 [07-09-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/itsm-testing/adapter-salesforce!14

---

## 0.9.4 [03-27-2024]

* Changes made at 2024.03.27_14:11PM

See merge request itentialopensource/adapters/itsm-testing/adapter-salesforce!13

---

## 0.9.3 [03-13-2024]

* Changes made at 2024.03.13_15:09PM

See merge request itentialopensource/adapters/itsm-testing/adapter-salesforce!12

---

## 0.9.2 [03-11-2024]

* Changes made at 2024.03.11_14:50PM

See merge request itentialopensource/adapters/itsm-testing/adapter-salesforce!11

---

## 0.9.1 [02-28-2024]

* Changes made at 2024.02.28_11:16AM

See merge request itentialopensource/adapters/itsm-testing/adapter-salesforce!10

---

## 0.9.0 [12-31-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/itsm-testing/adapter-salesforce!9

---

## 0.8.0 [05-25-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/itsm-testing/adapter-salesforce!8

---

## 0.7.4 [03-13-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/itsm-testing/adapter-salesforce!7

---

## 0.7.3 [07-10-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/itsm-testing/adapter-salesforce!6

---

## 0.7.2 [01-14-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/itsm-testing/adapter-salesforce!5

---

## 0.7.1 [11-19-2019]

- Update the healthcheck url to one that has been used in the lab

See merge request itentialopensource/adapters/itsm-testing/adapter-salesforce!4

---

## 0.7.0 [11-08-2019]

- Update the adapter to the latest adapter foundation.
  - Updating to adapter-utils 4.24.3 (automatic)
  - Add sample token schemas (manual)
  - Adding placement property to getToken response schema (manual - before encrypt)
  - Adding sso default into action.json for getToken (manual - before response object)
  - Add new adapter properties for metrics & mock (save_metric, mongo and return_raw) (automatic - check place manual before stub)
  - Update sample properties to include new properties (manual)
  - Update integration test for raw mockdata (automatic)
  - Update test properties (manual)
  - Changes to artifactize (automatic)
  - Update type in sampleProperties so it is correct for the adapter (manual)
  - Update the readme (automatic)

See merge request itentialopensource/adapters/itsm-testing/adapter-salesforce!3

---

## 0.6.0 [09-16-2019]

- Update the adapter to the latest adapter foundation

See merge request itentialopensource/adapters/itsm-testing/adapter-salesforce!2

---
## 0.5.0 [07-30-2019] & 0.4.0 [07-18-2019] & 0.3.0 [07-16-2019] & 0.2.0 [07-16-2019]

- Updates to the adapter foundation

See merge request itentialopensource/adapters/adapter-salesforce!1

---

## 0.1.1 [05-03-2019]

- Initial Commit

See commit e3e0165

---
