# Salesforce

Vendor: Salesforce
Homepage: https://www.salesforce.com/

Product: Salesforce
Product Page: https://www.salesforce.com/

## Introduction
We classify Salesforce into the ITSM domain as it is a cloud-based customer relationship management (CRM) platform that helps businesses manage sales, customer service, marketing, and more.

## Why Integrate
The Salesforce adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Salesforce.

With this adapter you have the ability to perform operations with Salesforce on items such as:

- Products
- Subscribers
- Incidents

## Additional Product Documentation
The [API documents for Salesforce](https://developer.salesforce.com/docs/apis)